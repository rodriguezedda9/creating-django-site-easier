# GENERATOR DJANGO SITES

This is a project for generating quickly and easy Django sites

1. This step is run only once, if you are using debian testing run this code to install dependencies. If you're not searching in google.

    ~~~~bash
    sudo apt install python3-pip python3-venv
    ~~~~

2. Running *create_project.sh* with two arguments: _name_project_ and _name_site_ you create one project and one site in Django, in next way:

    ```bash
    sh create_project.sh name_project name_site
    ```

3. If you want to create more sites in the same project (it means this part is optional), you should run this script:

    ```bash
    sh create_site.sh name_project name_new_site
    ```

4. Now, for setting up in the file settings.py we'll replace the following things:

   * instead of:

   ```python
   TIME_ZONE = 'UTC'
   ```

    this:

   ```python
   TIME_ZONE = 'America/Bogota'
   ```

   * instead of:

   ```python
   LANGUAGE_CODE = 'en-us'
   ```

   this:

   ```python
   LANGUAGE_CODE = 'es-CO'
   ```

   * instead of:

   ```python
   STATIC_URL = '/static/'
   ```

   this:

   ```python
   STATIC_URL = '/static/'
   STATIC_ROOT = os.path.join(BASE_DIR, 'static')
   ```

   * instead of:

   ```python
   ALLOWED_HOSTS = []
   ```

   this:

   ```python
   ALLOWED_HOSTS = ['127.0.0.1', '.pythonanywhere.com']
   ```

5. To configure the database in any engine, we should replace this code in setting.py file. Remember this part is already set up by default with sqlite3. This information is saved in file named change_database_system.txt.

   * instead of:
    ```python
    DATABASES = {
        'default': {
        'ENGINE':'django.db.backends.sqlite3',
    'NAME': 'mydatabase',
        }
    }
    ```


    this one if you prefer postgresql:
    ```python
    DATABASES = {
        'default': {
        'ENGINE':'django.db.backends.postgresql',
    'NAME': 'mydatabase',
    'USER': 'mydatabaseuser',
    'PASSWORD': 'mypassword',
    'HOST': '127.0.0.1',
    'PORT': '5432',
        }
    }
    ```

    this one if you prefer mysql:
    ```python
    DATABASES = {
        'default': {
        'ENGINE':'django.db.backends.mysql',
    'NAME': 'mydatabase',
    'USER': 'mydatabaseuser',
    'PASSWORD': 'mypassword',
    'HOST': '127.0.0.1',
    'PORT': '3306',
        }
    }
    ```
6. After this procedure you must run the next two commands to get the site, you can see:

   ```bash
   sh migrate.sh name_project/ name_site
   sh start_server.sh name_project/ name_site
   ```

   Django site: ![Alt](./imagenes/django_site.png "My first django site")


