#!/usr/bin/bash
# Before USE THIS RUN:
# sudo apt install python3-pip python3-venv
PROJECT_NAME=$1
SITE_NAME=$2
mkdir $PROJECT_NAME
cd $PROJECT_NAME
python3 -m venv myvenv
. myvenv/bin/activate
python -m pip install --upgrade pip
pip install -r ../requirements.txt
django-admin startproject $SITE_NAME .
